import asyncio
import dns.message
import dns.name
import dns.rdatatype
import dns.query
import slackweb
from fastapi import FastAPI, Form, Response, BackgroundTasks


app = FastAPI()


async def query_dns(name: str, url: str):
    name = dns.name.from_text(name)
    query = dns.message.make_query(name, dns.rdatatype.A)
    await asyncio.sleep(5)
    msg = dns.query.udp(query, "8.8.8.8")
    slack = slackweb.Slack(url)
    slack.notify(text=f"Answer for `A` of `{name}`\n```{msg}```")


@app.post("/slash-commands/dig")
async def dig(bg: BackgroundTasks, text: str = Form(""), response_url: str = Form("")):
    bg.add_task(query_dns, text, response_url)  # 直接処理せずに、移譲する
    return Response()
