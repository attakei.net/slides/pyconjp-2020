import dns.message
import dns.name
import dns.rdatatype
import dns.query
from fastapi import FastAPI, Form


app = FastAPI()


@app.post("/slash-commands/dig")
async def dig(text: str = Form("")):
    name = dns.name.from_text(text)
    query = dns.message.make_query(name, dns.rdatatype.A)
    msg = dns.query.udp(query, "8.8.8.8")
    return {
        "text": f"Answer for `A` of `{name}`\n```{msg}```"
    }
