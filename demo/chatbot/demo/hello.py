from fastapi import FastAPI, Form


app = FastAPI()


@app.post("/slash-commands/hello")
async def hello(text: str = Form(""), user_name: str = Form(...)):
    return {
        "text": f"Hello {user_name}, I recieved `{text}`"
    }
