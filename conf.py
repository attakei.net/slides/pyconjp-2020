# -- Path setup --------------------------------------------------------------
from pathlib import Path

import sass
from sphinx.ext.todo import todo_node, visit_todo_node, depart_todo_node

# -- Prepared proc -----------------------------------------------------------
here = Path(__file__).parent

# -- Project information -----------------------------------------------------
project = "pyconjp-2020"
copyright = "2020, Kazuya Takei"
author = "Kazuya Takei"

# The full version, including alpha/beta/rc tags
release = "2020.7"

# -- General configuration ---------------------------------------------------
extensions = [
    "sphinx.ext.todo",
    "sphinx_revealjs",
    "sphinxemoji.sphinxemoji",
]
templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store", "_includes", ".venv", "dist", "demo"]

# -- Extention configurations ----------
# sphinx.ext.todo
todo_include_todos = True

# sphinx_revealjs
revealjs_static_path = [
    "_static",
]
revealjs_css_files = [
    "css/custom.css",
    "revealjs/lib/css/zenburn.css",
]
revealjs_style_theme = "solarized" 
revealjs_google_fonts = [
    "M PLUS 1p",
]
revealjs_script_conf = """
{
    controls: false,
    hash: true,
    center: false,
    transition: 'none',
}
"""
revealjs_script_plugins = [
    {
        "src": "revealjs/plugin/notes/notes.js",
    },
    {
        "src": "revealjs/plugin/highlight/highlight.js",
        "options": """
            {async: true, callback: function() { hljs.initHighlightingOnLoad(); } }
        """
    },
]


def setup(app):
    app.add_node(
        todo_node,
        html=(visit_todo_node, depart_todo_node),
        revealjs=(visit_todo_node, depart_todo_node),
    )
    (here / "index.rst").touch()
    sass_dir = here / "_sass"
    css_dir = here / "_static/css"
    sass.compile(dirname=(sass_dir, css_dir))
